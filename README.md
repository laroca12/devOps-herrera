# devOps-herrera

devOps-herrera

# clonando Rama develop
* git clone git@gitlab.com:laroca12/devOps-herrera.git
## Realizando Cambios en archivo Readme
* git add .
* git commit -m "add folder src and modified archivo readme"
* git push origin develop

# Creando una nueva rama y realizar cambios
* git checkout -b mherrera
* git add src/main/java/com/dharbor/testdevops/config/SwaggerConfig.java
* git commit -m "add SwaggerConfig.java"
* git push origin mherrera

# Creando segunda rama
* git checkout -b userMarco
* git pull origin develop
* git add .
* git commit -m "add archivo import.sql into Resource"
* git push origin userMarco

